import React, { Component, Fragment } from 'react';


class Image extends Component {
    
    constructor(props){
        super(props);

        this.bootstrapper = this.bootstrapper.bind(this)
    }
    componentDidMount() {

    }

    bootstrapper(){
        this.warpHorizontally(this.providedIamge)
    }

    getQuadraticBezierXYatT(start_point, control_point, end_point, T) {

		let pow1minusTsquared = Math.pow(1 - T, 2),
			powTsquared = Math.pow(T, 2);

		let x = pow1minusTsquared * start_point.x + 2 * (1 - T) * T * control_point.x + powTsquared * end_point.x,
			y = pow1minusTsquared * start_point.y + 2 * (1 - T) * T * control_point.y + powTsquared * end_point.y; 
		
		return {
			x: x,
			y: y
		};
    }
    
    warpHorizontally (image_to_warp, invert_curve) {

		let image_width  = image_to_warp.width,
			image_height = image_to_warp.height,
			warp_percentage = parseFloat(1 /* this should provided by user */),
			// for fun purposes and nicer controls
			// I chose to determine the offset by applying a percentage value to the image width
			warp_x_offset = warp_percentage * image_width;

		this.wrappedImageCanvas.width  = image_width + Math.ceil(warp_x_offset * 2); 
        this.wrappedImageCanvas.height = image_height;
		let warp_context = this.wrappedImageCanvas.getContext('2d');

		// see https://www.rgraph.net/blog/an-example-of-the-html5-canvas-quadraticcurveto-function.html
		// for more details regarding start_point, control_point si end_point
		let start_point = {
			x: 0,
			y: 0
		};
		let control_point = {
			x: invert_curve ? warp_x_offset : -warp_x_offset,
			y: image_height / 2
		};
		let end_point = {
			x: 0,
			y: image_height
		};

		let offset_x_points = [],
			t = 0;
		for ( ; t < image_height; t++ ) {
			let xyAtT = this.getQuadraticBezierXYatT(start_point, control_point, end_point, t / image_height),
				x = parseInt(xyAtT.x);

			offset_x_points.push(x);
		}

		warp_context.clearRect(0, 0, this.wrappedImageCanvas.width, this.wrappedImageCanvas.height);

		let y = 0;
		for ( ; y < image_height; y++ ) {

			warp_context.drawImage(image_to_warp,
				// clip 1 pixel wide slice from the image
				//x, 0, 1, image_height + warp_y_offset,
				0, y, image_width + warp_x_offset, 1,
				// draw that slice with a y-offset
				//x, warp_y_offset + offset_y_points[x], 1, image_height + warp_y_offset
				warp_x_offset + offset_x_points[y], y, image_width + warp_x_offset, 1
			);
		}
    }
    
    warpVertically (image_to_warp, invert_curve) {
        console.log("image_to_warp", image_to_warp)
		let image_width  = image_to_warp.width,
			image_height = image_to_warp.height,
			warp_percentage = parseFloat(1 /* this should provided by user */, 10),
			// for fun purposes and nicer controls
			// I chose to determine the offset by applying a percentage value to the image height
			warp_y_offset = warp_percentage * image_height;
        this.wrappedImageCanvas.width  = image_width;
        this.wrappedImageCanvas.height = image_height + Math.ceil(warp_y_offset * 2); 
		let warp_context = this.wrappedImageCanvas.getContext('2d')
		
		console.log("sad")
		console.log("image_width",image_width,"\nimage_height",image_height,
		"\nwarp_percentage",warp_percentage,"\nwarp_y_offset",warp_y_offset,
		"\nthis.wrappedImageCanvas.width",this.wrappedImageCanvas.width,
		"\nthis.wrappedImageCanvas.height",this.wrappedImageCanvas.height)

		// see https://www.rgraph.net/blog/an-example-of-the-html5-canvas-quadraticcurveto-function.html
		// for more details regarding start_point, control_point si end_point
		let start_point = {
			x: 0,
			y: 0
		};
		let control_point = {
			x: image_width / 2,
			y: invert_curve ? warp_y_offset : -warp_y_offset
		};
		let end_point = {
			x: image_width,
			y: 0
		};
		
		let offset_y_points = [],
			t = 0;
		for ( ; t < image_width; t++ ) {
			let xyAtT = this.getQuadraticBezierXYatT(start_point, control_point, end_point, t / image_width),
				y = parseInt(xyAtT.y);

			offset_y_points.push(y);
		}
		console.log("offset_y_points length", offset_y_points.length)
		warp_context.clearRect(0, 0, this.wrappedImageCanvas.width, this.wrappedImageCanvas.height);

		let x = 0;
		for ( ; x < image_width; x++ ) {

			warp_context.drawImage(image_to_warp,
				// clip 1 pixel wide slice from the image
				x, 0, 1, image_height + warp_y_offset,
				// draw that slice with a y-offset
				x, warp_y_offset + offset_y_points[x], 1, image_height + warp_y_offset
			);
        }
	}

    render() {

        return(
            <>
                <img 
                    src={require("D:/Projects/Mandala/front/src/cropped_ibb_header-2.jpg")} 
                    alt={"person-using-computer"} style={{width:'300px', height:'300px', display:'block'}}
                    ref={(ref) => (this.providedIamge = ref)}
                    onLoad={this.bootstrapper}></img>
                <canvas
                    ref={(ref) => (this.wrappedImageCanvas = ref)}
                    style={{border: '1px solid #000000', margin: '5%'}}
                >
                </canvas>
            </>
        )
    }
}

export default Image

/*
https://stackoverflow.com/questions/7251177/curving-an-image-that-starts-as-a-rectangle-uploaded-by-user-preferably-using
*/