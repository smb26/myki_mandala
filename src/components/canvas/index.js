import React, { Component, Fragment } from 'react';

//
import MainButton from '../buttons/main_button';
import RelatedButton from '../buttons/related_button';
import Image from '../testing_implementation/images'
import { updateExpression } from '@babel/types';


let increaseSizeInterval = undefined;
let increaseSizeBoolean = undefined;

class Canvas extends Component {
    state = {
        layersRelatedButtons : [],
        initialCircleRadius: 50,
        drawnObjects: [],
        id: 0
    }
    constructor(props) {
        super(props);
        //this.initTool = this.initTool.bind(this);
        this.onMouseDown = this.onMouseDown.bind(this);
        this.onMouseMove = this.onMouseMove.bind(this);
        this.onDebouncedMove = this.onDebouncedMove.bind(this);
        this.onMouseUp = this.onMouseUp.bind(this);
        this.buttonClick = this.buttonClick.bind(this);
        this.increaseSize = this.increaseSize.bind(this);
        this.increaseSizeMouseDown = this.increaseSizeMouseDown.bind(this);
        this.increaseSizeMouseUp = this.increaseSizeMouseUp.bind(this);
    }

    

    componentDidMount() {

        this.setState({layersRelatedButtons : [
            {
                title: '+',
                function: this.increaseSize,
                functionOnMouseDown: this.increaseSizeMouseDown,
                functionOnMouseUp: this.increaseSizeMouseUp
            },
            {
                title: '-',
                function: this.decreaseSize
            }
        ]})
        // Here we set up the properties of the canvas element. 
        this.canvas.width = 800;
        this.canvas.height = 600;
        this.hiddenCanvas.width = 800;
        this.hiddenCanvas.height = 600;
        
        let ctx = this.canvas.getContext('2d');
        ctx.lineJoin = 'round';
        ctx.lineCap = 'round';
        ctx.lineWidth = 3;
        ctx.moveTo(0, 0);
        ctx.lineTo(200, 100);
        this.setState({
            drawnObjects: [
                ...this.state.drawnObjects,
                {
                    type: 'line',
                    startIndex: [0,0],
                    xyz: [200,100]
                }
            ]
        })
        ctx.stroke();
        const imgData = ctx.getImageData(10, 10, 50, 50);
        console.log(imgData.data);
        this.occupiedPixelsByShape(imgData.data);
        
    }

    onMouseDown() {

    }

    onMouseMove() {

    }

    onDebouncedMove() {

    }

    onMouseUp() {

    }

    buttonClick() {
        let ctx = this.canvas.getContext('2d');
        ctx.beginPath();
        /**
         * context.arc(x,y,r,sAngle,eAngle,counterclockwise);
         * The x-coordinate of the center of the circle
         * The y-coordinate of the center of the circle
         * The radius of the circle
         * The starting angle, in radians (0 is at the 3 o'clock position of the arc's circle)
         * The ending angle, in radians
         * Optional. Specifies whether the drawing should be counterclockwise or clockwise. False is default, and indicates clockwise, while true indicates counter-clockwise. 
         */
        let rnd = Math.random();
        let x = Math.floor(rnd*200);
        let y = Math.floor(rnd*200);
        let circleRadius = Math.floor(rnd*50);
        ctx.arc(x, y, circleRadius, 0, 2 * Math.PI);
        ctx.strokeStyle = 'rgb(0,0,0)';
        ctx.lineWidth = 3;
        this.setState({
            drawnObjects: [
                ...this.state.drawnObjects,
                {
                    type: 'arc',
                    startIndex: [x,y],
                    radius: circleRadius,
                    startEndAngle: [0, 2 * Math.PI]
                }
            ]
        });
        ctx.stroke(); 
    }

    async increaseSize() {
        let _length = this.state.drawnObjects.length
        let lastShape = _length>0 ? this.state.drawnObjects[_length-1] : null
        if(!lastShape) return;
        let arcShape = {
            type: 'arc',
            startIndex: [lastShape.startIndex[0],lastShape.startIndex[1]],
            radius: lastShape.radius+1,
            startEndAngle: [0, 2 * Math.PI]
        }

        // console.log('before calling updateCanvas',this.state.drawnObjects);
        await this.updateCanvas(arcShape);
        // console.log('after calling updateCanvas',this.state.drawnObjects);
        this.redrawCanvas();
        /* A */
        let ctx = this.canvas.getContext('2d');
        ctx.beginPath();
        ctx.arc(lastShape.startIndex[0], lastShape.startIndex[1], lastShape.radius+1, 0, 2 * Math.PI);
        ctx.strokeStyle = 'rgb(0,0,0)';
        ctx.lineWidth = 3;
        /* B */
        ctx.stroke();
        /* C */
        // if you put the lines of code that fall between A & B  OR between A & C before the call to this.redrawCanvas() 
        // then your last shape will not be added  
    }

    increaseSizeMouseDown() {
        increaseSizeInterval = setInterval(this.increaseSize, 100);
    }

    increaseSizeMouseUp() {
        clearInterval(increaseSizeInterval)
    }

    updateCanvas(shape){
        this.setState({
            drawnObjects: 
                [
                    ...this.state.drawnObjects.slice(0, this.state.drawnObjects.length-1),
                    shape
                ]
        })
    }

    redrawCanvas(){
        // delete everything that is on canvas currently
        let ctx = this.canvas.getContext('2d');
        ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        // in here we will go over the shapes that are stored in the state under the object 'drawnObjects'
        let shapes = [...this.state.drawnObjects];
        for(let i=0; i<shapes.length-1; i++){
            // if the shape is an arc then we will call the arc paint method
            // if the shape is a line then we will call the line paint method
            switch (shapes[i].type){
                case "arc":
                    this.drawArc(shapes[i]);
                    break;
                case "line":
                    this.drawLine(shapes[i]);
                    break;
                default:
                    console.log("Doesn't Match Any Shape That is defined")
            }
        }
    }

    occupiedPixelsByShape(data){
        console.log(data[31])
        let pixelsPosititions = [...data].map( (pixel, index) => {
            return pixel != 0 ? [pixel, index] : undefined;
        }).filter(pixel => {
            return pixel != undefined;
        })
        console.log("pixelsPosititions",pixelsPosititions)
        return pixelsPosititions;
    }

    drawArc(arc){
        let ctx = this.canvas.getContext('2d');
        ctx.beginPath();
        ctx.arc(arc.startIndex[0], arc.startIndex[1], arc.radius, 0, 2 * Math.PI);
        ctx.strokeStyle = 'rgb(0,0,0)';
        ctx.lineWidth = 3;
        ctx.stroke();
    }

    drawLine(line){

    }

    render() {
        return(
        <>
            <canvas
                ref={(ref) => (this.canvas = ref) }
                style={{border: '1px solid #000000', margin: '5%'}}
                onMouseDown={this.onMouseDown}
                onMouseMove={this.onMouseMove}
                onMouseOut={this.onMouseUp}
                onMouseUp={this.onMouseUp}
            >
            </canvas>
            {/* Will have the same dimensions and position of the NON Hidden/Invisible canvas
                The Hidden/Invisible canvas will only be responsible for one thing:
                Drawing a single shape on it, then getting the pixels that are occupied by the shape
                */}
            <canvas
                ref={(ref) => (this.hiddenCanvas = ref)}
                style={{border: '1px solid #000000', margin: '5%'}}
            ></canvas>

            <MainButton
                buttonClick={this.buttonClick}
            ></MainButton>
            <RelatedButton
                buttons={this.state.layersRelatedButtons}
            ></RelatedButton>
            {/*<Image></Image>*/}
        </>
        )
    }
}

export default Canvas;