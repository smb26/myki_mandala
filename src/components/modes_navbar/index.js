import React from 'react';

function ModesNavbar() {
  return (
    <div>
        <table>
            <th>Simple</th>
            <th>--------</th>
            <th>Intermediate</th>
            <th>--------</th>
            <th>Creative</th>
            <th>--------</th>
            <th>FreeStyla</th>
        </table>
    </div>
  );
}

export default ModesNavbar;