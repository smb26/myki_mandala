import React, { Component, Fragment } from 'react';



let paint = undefined;
let clickX = new Array(new Array()); 
// 2D array that will have the X coordinates of each slice of the mirror, so if the user decided to have 12 mirroring slices
// then the there will be 12 arrays, where each array/slice will have its own X coordinates
let clickY = new Array(new Array());
// same concept as clickX 
let clickDrag = new Array();
let stoppingPointX = undefined; // will specify where we last stopped drawing at in terms of X
let stoppingPointY = undefined; // will specify where we last stopped drawing at in terms of Y

class SymmetricCanvas extends Component {
    state = {
        layersRelatedButtons : [],
        nFoldRotationSymmetry: 10,
        mirrorSymmetry: false
    }
    constructor(props) {
        super(props);
        //this.initTool = this.initTool.bind(this);
        this.onMouseDown = this.onMouseDown.bind(this);
        this.onMouseMove = this.onMouseMove.bind(this);
        this.onMouseUp = this.onMouseUp.bind(this);
        this.onMouseLeave = this.onMouseLeave.bind(this);
    }

    

    componentDidMount() {
        // Here we set up the properties of the canvas element. 
        this.canvas.width = 700;
        this.canvas.height = 700;
        
    }

    onMouseDown(e) {
        /*
        The HTMLElement.offsetLeft read-only property returns the number of 
        pixels that the upper left corner of the current element is offset to
         the left within the HTMLElement.offsetParent node.
        */
        let mouseX = e.pageX - this.canvas.offsetLeft;
        let mouseY = e.pageY - this.canvas.offsetTop;
        console.log("mouseX", mouseX, "\nmouseY", mouseY);
        this.paint = true;
        this.addClick(e.pageX - this.canvas.offsetLeft, e.pageY - this.canvas.offsetTop, undefined, 0);
        const setOfXYpoints = this.getMirroringPoints(e.pageX - this.canvas.offsetLeft, e.pageY - this.canvas.offsetTop);
        for(let i=0; i<setOfXYpoints.length; i++){
            this.addClick(setOfXYpoints[i][0], setOfXYpoints[i][1], undefined,i+1);
        }
        this.continueDraw();
    }

    onMouseMove(e) {
        if(this.paint){
            this.addClick(e.pageX - this.canvas.offsetLeft, e.pageY - this.canvas.offsetTop, true,0);
            const setOfXYpoints = this.getMirroringPoints(e.pageX - this.canvas.offsetLeft, e.pageY - this.canvas.offsetTop);
            
            for(let i=0; i<setOfXYpoints.length; i++){
                this.addClick(setOfXYpoints[i][0], setOfXYpoints[i][1], true, i+1);
            }
            this.continueDraw();
        }
    }

    onMouseUp() {
        this.paint = false;
    }

    onMouseLeave() {
        this.paint = false;
    }

    addClick(X, Y, bool, i){
        if(clickX[i]==undefined){
            clickX.push([]);
        }
        if(clickY[i]==undefined){
            clickY.push([]);
        }
        clickX[i].push(X);
        clickY[i].push(Y);
        clickDrag.push(bool);
    }

    getMirroringPoints(x,y){
        let centerX = this.canvas.width / 2;
        let centerY = this.canvas.height / 2;
        /*console.log("x", x);
        console.log("centerX",centerX);
        console.log("y", y);
        console.log("centerY",centerY)
        */let xRelativeToCanvas = x - centerX; // this is the point x relative to the canvas
        let yRelativeToCanvas = centerY - y; // this is the point y relative to the canvas
        /*console.log("xRelativeToCanvas", xRelativeToCanvas);
        console.log("yRelativeToCanvas", yRelativeToCanvas);
        */let dist  = Math.hypot(xRelativeToCanvas, yRelativeToCanvas);
        /*
            The Math.atan2() function returns the angle in the plane (in radians) 
            between the positive x-axis and the ray from (0,0) to the point (x,y),
            for Math.atan2(y,x)
        */
        let angle = Math.atan2(xRelativeToCanvas, yRelativeToCanvas);

        let points = []
        let i =0
        for(; i < this.state.nFoldRotationSymmetry; i++){
            var theta = angle + Math.PI * 2 / this.state.nFoldRotationSymmetry * i;  // Radians
            x = centerX + Math.sin(theta) * dist;
            y = centerY - Math.cos(theta) * dist;
            points.push([x, y]);
            if (this.state.mirrorSymmetry) {
                x = centerX - Math.sin(theta) * dist;
                points.push([x, y]);
            }
        }
        return points;
        
    }

    continueDraw(){
        let context = this.canvas.getContext('2d');
        context.strokeStyle = "#df4b26";
        context.lineJoin = "round";
        context.lineWidth = 1.2;
        let j=0;
        // if stoppingPointX is still undefined then this is our first ever entry into continueDraw function
        if (this.stoppingPointX!=undefined){
            // but if stoppingPointX has a value then we want to continue drawing from that value
            j=this.stoppingPointX.length;
        }
        for(; j<clickX.length; j++){
            for(let i=0; i < clickX[j].length; i++) {	
                context.beginPath();
                if(clickDrag[i] && i){
                    context.moveTo(clickX[j][i-1], clickY[j][i-1]);
                }else{
                    context.moveTo(clickX[j][i]-1, clickY[j][i]);
                }
                context.lineTo(clickX[j][i], clickY[j][i]);
                context.closePath();
                context.stroke();
            }
        }
        stoppingPointX = clickX[j-1].length-1;
        stoppingPointY = clickY[j-1].length-1;
    }

    redraw(){
        let context = this.canvas.getContext('2d');
        context.clearRect(0, 0, context.canvas.width, context.canvas.height); // Clears the canvas
        context.strokeStyle = "#df4b26";
        context.lineJoin = "round";
        context.lineWidth = 1.2;
        for(var i=0; i < clickX.length; i++) {		
            context.beginPath();
            if(clickDrag[i] && i){
                context.moveTo(clickX[i-1], clickY[i-1]);
            }else{
                context.moveTo(clickX[i]-1, clickY[i]);
            }
            context.lineTo(clickX[i], clickY[i]);
            context.closePath();
            context.stroke();
        }
          
    }


    render() {
        return(
        <>
            <canvas
                ref={(ref) => (this.canvas = ref) }
                style={{border: '1px solid #000000', margin: '5%'}}
                onMouseDown={this.onMouseDown}
                onMouseMove={this.onMouseMove}
                onMouseOut={this.onMouseUp}
                onMouseLeave={this.onMouseLeave}
                onMouseUp={this.onMouseUp}
            >
            </canvas>
            {/*<Image></Image>*/}
        </>
        )
    }
}

export default SymmetricCanvas;