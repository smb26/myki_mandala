import React, {Component, Fragment} from 'react';

class RelatedButton extends Component {

    constructor(props){
        super(props)
    }

    componentDidMount() {

    }
    render() {
        const {buttons} = this.props;
        return (
            <>
                {buttons && buttons.map( (elm) => {
                    return <button
                                onClick={e => elm.function(e)}
                                onMouseDown={e => elm.functionOnMouseDown()}
                                onMouseUp={e => elm.functionOnMouseUp()}
                            > {elm.title} </button>
                })}
            </>
        )
    }
}

export default RelatedButton;