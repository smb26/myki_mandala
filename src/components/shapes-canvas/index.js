import React, { Component, Fragment } from "react";
import ex_img from '../../single_shape_1.png';
import { array } from "prop-types";

class ShapesCanvas extends Component {
    state = {
        nFoldRotationSymmetry: 10,
        mirrorSymmetry: false,
        imgPath: null
    }

    constructor(){
        super();
        this.bootstrapper = this.bootstrapper.bind(this);
    }
    

    componentDidMount(){
    }

    bootstrapper(){
        this.convertImageToCanvas(this.providedIamge);
        this.white2transparent();
    }

    convertImageToCanvas(image) {
        let canvasConext = this.canvas.getContext("2d");
        this.canvas.width = image.width;
        this.canvas.height = image.height;
        this.canvas.getContext("2d").drawImage(image, 0, 0);
        let imgData = canvasConext.getImageData(0,0,this.canvas.width,this.canvas.height);
        console.log(imgData);
        this.getShapePixelLocations(imgData)
        // console.log("imgData red",imgData.data[0]);
        // console.log("imgData green",imgData.data[1]);
        // console.log("imgData blue",imgData.data[2]);
        //this.occupiedPixelsByShape(imgData.data)
    }

    getShapePixelLocations(data){
        let pixels = [...data.data];
        let heightOfImage = data.height;
        let widthOfImage = data.width;
        let lengthOfPixels = pixels.length;
        let shapeRGB_Values = new Array(lengthOfPixels/4);
        let heightCroppingIndex = -1;
        let widthCroppingIndex = -1;
        //let shapeRGB_ValuesObject = {}
        //console.log("shapeRGB_Values before any modification", shapeRGB_Values);
        //console.log("heightOfImage", heightOfImage);
        //console.log("widthOfImage", widthOfImage);
        //console.log("lengthOfPixels", lengthOfPixels);
        for (let i=0; i<pixels.length; i+=4){
            if (pixels[i]!=255 || pixels[i+1]!=255 || pixels[i+2]!=255 || pixels[i+3]!=255){
                shapeRGB_Values[i/4] = [pixels[i], pixels[i+1], pixels[i+2], pixels[i+3]];
                heightCroppingIndex = i/4;
                // checking where the colored pixel lies in terms of x-axis
                // if this is the biggest value we encountered till now then we put it in widthCroppingIndex
                let x_value = (i/4)%data.width;
                if(x_value>widthCroppingIndex) widthCroppingIndex = x_value
            }else{
                shapeRGB_Values[i/4] = -1;
            }
        }
        //console.log("heightCroppingIndex",heightCroppingIndex);
        //console.log("shapeRGB_Values",shapeRGB_Values);
        let sliced_ShapeRGB_Values = shapeRGB_Values.slice(0,heightCroppingIndex);
        //console.log("sliced_ShapeRGB_Values",sliced_ShapeRGB_Values);
        //outputCanvasContext.
        this.outputShapeOnCanvas(sliced_ShapeRGB_Values, heightCroppingIndex, widthCroppingIndex, data.height, data.width);
    }

    outputShapeOnCanvas(data,y_indexOfLastColoredPixel,x_indexOfLastColoredPixel, height, width){
        console.log("height", height);
        console.log("width", width);
        let heightCroppingPoint = Math.ceil(y_indexOfLastColoredPixel/width);
        console.log("heightCroppingPoint",heightCroppingPoint);
        let context = this.outputCanvas.getContext("2d");
        this.outputCanvas.width = this.providedIamge.width;
        this.outputCanvas.height = this.providedIamge.height;
        let imgData = context.getImageData(0,0,this.outputCanvas.width,this.outputCanvas.height);
        console.log("imgData.data.length",imgData.data.length);

        for(let i=0; i<imgData.data.length; i+=4){
            if(data[i/4]!=-1 && data[i/4]){
                imgData.data[i] = data[i/4][0]; // Red
                imgData.data[i+1] = data[i/4][1]; // Green
                imgData.data[i+2] = data[i/4][2]; // Blue
                imgData.data[i+3] = data[i/4][3]; // Alpha
            }else{
                imgData.data[i] = 255; // Red
                imgData.data[i+1] = 255; // Green
                imgData.data[i+2] = 255; // Blue
                imgData.data[i+3] = 255; // Alpha
            }
        }
        console.log(imgData);
        context.putImageData(imgData,0,0);
        imgData = context.getImageData(0,0,x_indexOfLastColoredPixel,heightCroppingPoint);
        this.finalResultCanvas.width = x_indexOfLastColoredPixel;
        this.finalResultCanvas.height = heightCroppingPoint;
        let finalCanvasContext = this.finalResultCanvas.getContext("2d");
        finalCanvasContext.putImageData(imgData,0,0)

    }

    cropCanvas(sourceCanvas,left,top,width,height) {
        // https://stackoverflow.com/questions/13073647/crop-canvas-export-html5-canvas-with-certain-width-and-height
        let destCanvas = document.createElement('canvas');
        destCanvas.width = width;
        destCanvas.height = height;
        destCanvas.getContext("2d").drawImage(
            sourceCanvas,
            left,top,width,height,  // source rect with content to crop
            0,0,width,height);      // newCanvas, same size as source rect
        return destCanvas;
    }

    white2transparent()
    {


        this.transparentCanvas.width = this.providedIamge.width;
        this.transparentCanvas.height = this.providedIamge.height;

        var ctx = this.transparentCanvas.getContext("2d");

        ctx.drawImage(this.providedIamge, 0, 0, this.providedIamge.width, this.providedIamge.height);
        var imageData = ctx.getImageData(0,0, this.providedIamge.width, this.providedIamge.height);
        var pixel = imageData.data;

        var r=0, g=1, b=2,a=3;
        for (var p = 0; p<pixel.length; p+=4) {
            if (pixel[p+r] == 255 &&
                pixel[p+g] == 255 &&
                pixel[p+b] == 255){
                    pixel[p+a] = 0;
                } // if white then change alpha to 0
        }

        ctx.putImageData(imageData,0,0);

        this.setState({
            imgPath: this.transparentCanvas.toDataURL('image/png')
        })
        
    }

    occupiedPixelsByShape(data){
        console.log(data[31])
        let pixelsPosititions = [...data].map( (pixel, index) => {
            return pixel != 0 ? [pixel, index] : undefined;
        }).filter(pixel => {
            return pixel != undefined;
        })
        console.log("pixelsPosititions",pixelsPosititions)
        return pixelsPosititions;
    }

    render(){
        return(
            <>
                <canvas
                    ref={(ref) => (this.canvas = ref) }
                    style={{border: '1px solid #000000', margin: '5%'}}
                    onMouseDown={this.onMouseDown}
                    onMouseMove={this.onMouseMove}
                    onMouseOut={this.onMouseUp}
                    onMouseLeave={this.onMouseLeave}
                    onMouseUp={this.onMouseUp}
                >
                </canvas>
                
                <img src={ex_img} alt="dakf"
                     ref={(ref) => (this.providedIamge = ref)}
                     onLoad={this.bootstrapper}></img>
                
                <canvas
                    ref={(ref) => (this.outputCanvas = ref) }
                    style={{border: '1px solid #000000', margin: '5%'}}
                    onMouseDown={this.onMouseDown}
                    onMouseMove={this.onMouseMove}
                    onMouseOut={this.onMouseUp}
                    onMouseLeave={this.onMouseLeave}
                    onMouseUp={this.onMouseUp}
                >
                </canvas>

                <canvas
                    ref={(ref) => (this.finalResultCanvas = ref) }
                    style={{border: '1px solid #000000', margin: '5%'}}
                >
                </canvas>

                <canvas
                    ref={(ref) => (this.transparentCanvas = ref) }
                    style={{border: '1px solid #000000', margin: '5%'}}
                    onMouseDown={this.onMouseDown}
                    onMouseMove={this.onMouseMove}
                    onMouseOut={this.onMouseUp}
                    onMouseLeave={this.onMouseLeave}
                    onMouseUp={this.onMouseUp}
                >
                </canvas>
                {this.state.imgPath && 
                    <img src={this.state.imgPath} alt="dakf"></img>
                }
            </>
        )
    }
}

export default ShapesCanvas