import React from 'react';
// Components
import ModesNavbar from './components/modes_navbar/index';
import Canvas from './components/canvas';
import SymmetricCanvas from './components/symmetric-canvas'
import ShapesCanvas from './components/shapes-canvas'
import './App.css';

function Entry() {
  return (
    <div className="App">
      <header >
        Mandala
      </header>
      <ModesNavbar></ModesNavbar>
      {/* <Canvas></Canvas>*/}
      {<SymmetricCanvas></SymmetricCanvas> }
      {/*<ShapesCanvas></ShapesCanvas>*/}
    </div>
  );
}

export default Entry;
